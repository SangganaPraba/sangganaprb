import React, { useState } from "react";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import Grid from '@material-ui/core/Grid';

export default function ButtProd() {
  const [quantity, setQuantity] = useState(0);
  function tambah() {
    setQuantity(quantity + 1);
  }
  function kurang() {
    if (quantity > 0) {
      setQuantity(quantity - 1);
    }
  }
    return (
        <Grid style={{ 
            display: "flex",
            alignItems: "flex-end"
            }}
          >
                {quantity == 0 && (
                  <Button
                    style={{
                      fontFamily: "Montserrat",
                      fontSize: "9px",
                      width: "80px",
                      height: "30px",
                      marginTop: "80px",
                      backgroundColor: "rgb(241, 91, 93)",
                      color: "rgb(255, 255, 255)"
                    }}
                    variant="contained"
                    onClick={tambah}
                  >
                    Tambahkan
                  </Button>
                )}
  
                {quantity > 0 && (
                  <>
                    <Button
                      size="small"
                      style={{ minWidth: 10, marginTop: "80px" }}
                      variant="outlined"
                      onClick={kurang}
                    >
                      -
                    </Button>
  
                    <Typography
                      style={{
                        marginLeft: "5px"
                      }}
                      variant="body2"
                      color="textSecondary"
                    >
                      {quantity}
                    </Typography>
  
                    <Button
                      size="small"
                      style={{ minWidth: 10, marginLeft: "5px" }}
                      variant="contained"
                      color="secondary"
                      onClick={tambah}
                    >
                      +
                    </Button>
                  </>
                )}
              </Grid>
    )
}
