import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import ArrowBackIosIcon from "@material-ui/icons/ArrowBackIos";
import IconButton from "@material-ui/core/IconButton";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import { Link } from "react-router-dom";

const useStyles = makeStyles((theme) => ({
  menuButton: {
    marginRight: theme.spacing(1),
    fontFamily: "Montserrat"
  },
  text: {
    marginTop: "30px",
    fontFamily: "Montserrat"
  }
}));

export default function Login() {
  const classes = useStyles();

  return (
    <div>
      <AppBar position="absolute" color="transparent" elevation={0}>
        <Toolbar>
          <IconButton
            component={Link}
            to="/"
            className={classes.menuButton}
            color="secondary"
          >
            <ArrowBackIosIcon />
          </IconButton>
          <Typography className={classes.menuButton} variant="h7">
            Masuk Akun
          </Typography>
        </Toolbar>
      </AppBar>
      <Grid
        style={{
          marginTop: "100px"
        }}
        container
        justify="center"
        alignItems="center"
      >
        <img src="https://tumbasin.id/static/media/masuk.097ee68d.svg" />
      </Grid>
      <Grid container justify="center" alignItems="center">
        <Typography className={classes.text} variant="h8" component="h4">
          Masuk
        </Typography>
      </Grid>
      <Grid container justify="center" alignItems="center">
        <Typography
          style={{
            fontFamily: "Montserrat",
            marginLeft: "10px"
          }}
          class="MuiTypography-caption MuiTypography-alignCenter"
          gutterBottom
        >
          Nikmati kepuasan dan kenyamanan kualitas <br />
          belanja kebutuhan sehari - hari dengan Tumbasin
        </Typography>
      </Grid>
      <br />
      <Grid
        style={{
          padding: "5px"
        }}
        container
        justify="center"
        alignItems="center"
      >
        <Button size="small" variant="contained" color="primary">
          <img
            src="https://tumbasin.id/static/media/google.1afb8f6b.svg"
            style={{
              width: "20px",
              Height: "20px",
              display: "inline-block",
              padding: "5px",
              paddingLeft: "1px",
              fontFamily: "Montserrat"
            }}
          />
          Sign in With Google
        </Button>
      </Grid>
      <br />
      <Grid container justify="center" alignItems="center">
        <Button size="small" variant="contained" color="secondary">
          <img
            src="https://tumbasin.id/static/media/emaillain.3fcfe399.svg"
            style={{
              width: "20px",
              Height: "20px",
              display: "inline-block",
              padding: "5px",
              paddingLeft: "1px",
              fontFamily: "Montserrat"
            }}
          />
          Sign Up With Email
        </Button>
      </Grid>
      <br />
      <Grid container justify="center" alignItems="center">
        <Typography
          style={{
            fontFamily: "Montserrat",
            fontSize: "10px"
          }}
          color="rgb(159, 163, 166)"
          class="MuiTypography-caption MuiTypography-alignCenter"
          gutterBottom
        >
          Dengan masuk dan mendaftar, Anda menyetujui
          <br />
          <b>Syarat Penggunaan</b> dan <b>Kebijakan Privasi</b>
        </Typography>
      </Grid>
    </div>
  );
}