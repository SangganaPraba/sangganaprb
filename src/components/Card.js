import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import StoreIcon from "@material-ui/icons/Store";

const useStyles = makeStyles({
    root: {
      width: "330px",
      height: "85px",
      marginTop: "15px",
      marginLeft: "15px"
    },
    title: {
      fontSize: 14,
      marginLeft: "10px"
    }
  });
  
  export default function Cardd() {
    const classes = useStyles();
  
    return (

      <Card className={classes.root} variant="outlined">
        <CardContent>
          <Grid container spacing={5}>
            <Grid item xs={11}>
              <Typography
                style={{
                  fontFamily: "Montserrat"
                }}
                className={classes.title}
                color="textSecondary"
                gutterBottom
              >
                Kamu Belanja Di :
              </Typography>
              <Grid container justify="space-between" alignItems="center">
                <StoreIcon
                  style={{
                    marginLeft: "10px",
                    color: "rgb(135, 202, 254)"
                  }}
                />
                <Typography
                  style={{
                    fontFamily: "Montserrat",
                    fontSize: 14
                  }}
                >
                 <b>Pasar Bulu Semarang</b>
                </Typography>
  
                <Grid>
                  <Button
                    style={{
                      fontFamily: "Montserrat",
                      backgroundColor: "rgb(241, 91, 93)",
                      color: "white"
                    }}
                    size="small"
                    variant="contained"
                  >
                    GANTI
                  </Button>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </CardContent>
      </Card>
    );
  }