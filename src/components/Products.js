import React, { useState, useEffect } from 'react';
import axios from "axios";
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import ButtProd from "./ButtProd";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  img: {
    width:80,
    height:80,
    marginLeft: "10px"
  },
  text: {
    fontFamily: "Montserrat",
    fontSize: "12px",
    marginLeft: "10px"
  }
}));

export default function Products() {
  const classes = useStyles();
  const [products, setProducts] = useState([])
   useEffect(() => {
       axios
         .get("https://api.tumbasin.id/t/v1/products?vendor=1003&featured=true")
         .then(response => setProducts(response.data));
     }, [])

  return (
    <div className={classes.root}>
      {products.slice(0,5).map((product) => (
      <Grid container spacing={3}>
        <Grid item xs={3}>
          <img className={classes.img} src={product.images[0].src}/>
        </Grid>
        <Grid item xs={6}>
        <p className={classes.text}>
          {product.name}
        </p><br/>
        <p className={classes.text}>
          <font color="red">Rp.{product.price}</font>
          <font color="#C7C7C9">/{product.meta_data[0].value}</font>
        </p>
        
        </Grid>
        <Grid item xs={3}>
          <ButtProd/>
        </Grid>
      </Grid>
      ))}
    </div>
  );
}
