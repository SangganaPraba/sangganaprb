import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";

const useStyles = makeStyles((theme) => ({
  User: {
    marginTop: "65px",
    background:
      "linear-gradient(180.97deg, rgb(209, 48, 50) -22.67%, rgb(227, 65, 67) -3.9%, rgb(237, 82, 84) 16.54%, rgb(241, 91, 93) 57.41%)"
  },
  teks: {
    color: "white",
    marginTop: "5px"
  }
}));

export default function Help() {
  const classes = useStyles();

  return (
    <>
      <Card
        className={classes.User}
        color="secondary"
        elevation={0}
        style={{
          borderRadius: 0
        }}
      >
        <Grid container alignItems="center" justify="center">
          <Typography
            style={{
              fontFamily: "Montserrat"
            }}
            className={classes.teks}
          >
            <p>Hello Users</p>
          </Typography>
          <Grid container alignItems="center" justify="center">
            <Typography
              style={{
                fontFamily: "Montserrat",
                fontSize: "20px"
              }}
              className={classes.teks}
            >
              <p>Anda Memerlukan Bantuan</p>
            </Typography>
          </Grid>
        </Grid>
        <AppBar position="absolute" color="transparent" elevation={0}>
          <Toolbar>
            <Typography
              style={{
                fontFamily: "Montserrat"
              }}
              className={classes.menuButton}
            >
              <b>Bantuan</b>
            </Typography>
          </Toolbar>
        </AppBar>
      </Card>
      <br />
      <div>
        <Typography
          style={{
            fontFamily: "Montserrat",
            fontSize: "15px",
            marginLeft: "10px"
          }}
        >
          <b>Tentang Kami</b>
        </Typography>
        <hr />
        <Typography
          style={{
            fontFamily: "Montserrat",
            fontSize: "15px",
            marginLeft: "10px"
          }}
        >
          Apa Tumbasin.id ?
        </Typography>
        <hr />
        <Typography
          style={{
            fontFamily: "Montserrat",
            fontSize: "15px",
            marginLeft: "10px"
          }}
        >
          <b>Operasional</b>
        </Typography>
        <hr />
        <Typography
          style={{
            fontFamily: "Montserrat",
            fontSize: "15px",
            marginLeft: "10px"
          }}
        >
          Pasar mana saja yang bekerja sama dengan Tumbasin.id ?
        </Typography>
        <hr />
        <Typography
          style={{
            fontFamily: "Montserrat",
            fontSize: "15px",
            marginLeft: "10px"
          }}
        >
          Operasional Tumbasin.id sudah ada dikota mana saja ?
        </Typography>
        <hr />
        <Typography
          style={{
            fontFamily: "Montserrat",
            fontSize: "15px",
            marginLeft: "10px"
          }}
        >
          <b>Harga dan Transaksi</b>
        </Typography>
        <hr />
        <Typography
          style={{
            fontFamily: "Montserrat",
            fontSize: "15px",
            marginLeft: "10px"
          }}
        >
          Berapa ongkos kirimnya ?
        </Typography>
        <hr />
        <Typography
          style={{
            fontFamily: "Montserrat",
            fontSize: "15px",
            marginLeft: "10px"
          }}
        >
          Kapan saja dapat menerima pengembalian uang dari Tumbasin.id ?
        </Typography>
        <hr />
        <Typography
          style={{
            fontFamily: "Montserrat",
            fontSize: "15px",
            marginLeft: "10px"
          }}
        >
          <b>Pesanan</b>
        </Typography>
        <hr />
        <Typography
          style={{
            fontFamily: "Montserrat",
            fontSize: "15px",
            marginLeft: "10px"
          }}
        >
          Seberapa cepat Tumbasin.id mengantarkan pesanan ?
        </Typography>
        <hr />
        <Typography
          style={{
            fontFamily: "Montserrat",
            fontSize: "15px",
            marginLeft: "10px"
          }}
        >
          Apa yang dimaksud waktu dengan pengantaran ?
        </Typography>
        <hr />
        <Typography
          style={{
            fontFamily: "Montserrat",
            fontSize: "15px",
            marginLeft: "10px"
          }}
        >
          Bagaimana jika stok barang yang saya pesan habis ?
        </Typography>
        <hr />
        <Typography
          style={{
            fontFamily: "Montserrat",
            fontSize: "15px",
            marginLeft: "10px"
          }}
        >
          Apakah harga barang di Tumbasin.id berbeda dengan harga di Pasar
          Tradisional ?
        </Typography>
        <hr />
        <Typography
          style={{
            fontFamily: "Montserrat",
            fontSize: "15px",
            marginLeft: "10px"
          }}
        >
          Bagaimana saya mengecek status pesanan saya ?
        </Typography>
        <hr />
        <Typography
          style={{
            fontFamily: "Montserrat",
            fontSize: "15px",
            marginLeft: "10px"
          }}
        >
          Bagaimana cara edit atau membatalkan pesanan ?
        </Typography>
        <hr />
        <Typography
          style={{
            fontFamily: "Montserrat",
            fontSize: "15px",
            marginLeft: "10px"
          }}
        >
          Apa kebijakan Tumbasin.id terkait pembatalan pesanan ?
        </Typography>
        <hr />
        <Typography
          style={{
            fontFamily: "Montserrat",
            fontSize: "15px",
            marginLeft: "10px"
          }}
        >
          Bagaimana jika saya ingin melaporkan masalah terhadap pesanan saya ?
        </Typography>
        <hr />
        <Typography
          style={{
            fontFamily: "Montserrat",
            fontSize: "15px",
            marginLeft: "10px"
          }}
        >
          Bagaimana melihat struk pembelanjaan saya ?
        </Typography>
        <hr />
        <Typography
          style={{
            fontFamily: "Montserrat",
            fontSize: "15px",
            marginLeft: "10px"
          }}
        >
          Bagaimana jika saya ingin mengembalikan kantong belanja Tumbasin.id ?
        </Typography>
        <hr />
        <Typography
          style={{
            fontFamily: "Montserrat",
            fontSize: "15px",
            marginLeft: "10px"
          }}
        >
          Bagaimana jika saya ingin meretur barang ?
        </Typography>
        <hr />
        <Typography
          style={{
            fontFamily: "Montserrat",
            fontSize: "15px",
            marginLeft: "10px"
          }}
        >
          Siapa yang akan memilihkan pesanan saya ?
        </Typography>
        <hr />
        <Typography
          style={{
            fontFamily: "Montserrat",
            fontSize: "15px",
            marginLeft: "10px"
          }}
        >
          Siapa yang akan mengantarkan pesanan saya ?
        </Typography>
        <hr />
        <Typography
          style={{
            fontFamily: "Montserrat",
            fontSize: "15px",
            marginLeft: "10px"
          }}
        >
          Saya mempunyai pertanyaan lebih lanjut untuk Tumbasin.id!
        </Typography>
        <hr />
        <Typography
          style={{
            fontFamily: "Montserrat",
            fontSize: "15px",
            marginLeft: "10px"
          }}
        >
          Masih <b>butuh bantuan</b> atau <b>punya pertanyaan lain</b> yang
          ingin ditanyakan? <font color="red">HUBUNGI KAMI</font>
        </Typography>
        <hr />
        <Grid
          container
          class="MuiGrid-root MuiGrid-container"
          style={{
            padding: "10px"
          }}
        >
          <Grid class="MuiGrid-root MuiGrid-item MuiGrid-grid-xs-1">
            <img
              style={{
                width: "20px",
                height: "30px"
              }}
              src="https://tumbasin.id/static/media/info.f0b746a7.svg"
            />
          </Grid>
          <Grid class="MuiGrid-root MuiGrid-item MuiGrid-grid-xs-11">
            <div>
              <Typography
                style={{
                  color: "rgb(137, 139, 140)",
                  fontFamily: "Montserrat",
                  fontSize: "13px",
                  marginBottom: "50px"
                }}
              >
                Layanan Pelanggan 24 Jam, Senin s/d Minggu, tidak termasuk Hari
                Libur Nasional.
              </Typography>
            </div>
          </Grid>
        </Grid>
      </div>
      <img
        style={{
          // backgroundRepeat: "no-repeat",
          right: "0",
          position: "fixed",
          bottom: "15px",
          marginBottom: "56px"
        }}
        alt=""
        src="https://tumbasin.id/static/media/wa.4c0c2a92.svg"
      />
    </>
  );
}
