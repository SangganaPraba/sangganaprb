import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import StorefrontIcon from "@material-ui/icons/Storefront";
import ReceiptIcon from "@material-ui/icons/Receipt";
import LiveHelpIcon from "@material-ui/icons/LiveHelp";
import PersonIcon from "@material-ui/icons/Person";
import BottomNavigation from "@material-ui/core/BottomNavigation";
import BottomNavigationAction from "@material-ui/core/BottomNavigationAction";
import { Link } from "react-router-dom";
import { Grid } from "@material-ui/core";

const useStyles = makeStyles({
    actionItem: {
      "&$selected": {
        color: "#F15B5D"
      }
    },
    selected: {},
    root: {
      width: 500
    }
  });

  export default function BottomNav() {
    const classes = useStyles();
    const [value, setValue] = React.useState(0);
    return (
      <Grid
      container
      alignItems="center"
      justify="center"
      style={{
        width: "100%",
        height: "40px"
      }}>
      <BottomNavigation
      class="MuiBottomNavigation-root"
        style={{
          width: "390px",
          position: "fixed",
          boxShadow: "0px 0px 2px #9e9e9e",
          bottom: "0",
          maxWidth: "400px",
          marginLeft: "10px",
          display: "flex"
        }}
        value={value}
        onChange={(event, newValue) => {
          setValue(newValue);
        }}
        showLabels
        className={classes.root}
      >
        <BottomNavigationAction
          classes={{
            root: classes.actionItem,
            selected: classes.selected
          }}
          component={Link}
          to="/"
          label="Belanja"
          icon={<StorefrontIcon />}
        />
  
        <BottomNavigationAction
          classes={{
            root: classes.actionItem,
            selected: classes.selected
          }}
          component={Link}
          to="/login"
          label="Transaksi"
          icon={<ReceiptIcon />}
        />
  
        <BottomNavigationAction
          classes={{
            root: classes.actionItem,
            selected: classes.selected
          }}
          component={Link}
          to="/help"
          label="Bantuan"
          icon={<LiveHelpIcon />}
        />
        <BottomNavigationAction
          classes={{
            root: classes.actionItem,
            selected: classes.selected
          }}
          component={Link}
          to="/login"
          label="Profile"
          icon={<PersonIcon />}
        />
      </BottomNavigation>
      </Grid>
    );
  }