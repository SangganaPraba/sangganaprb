import React from 'react';
import { makeStyles } from "@material-ui/core/styles";
import SearchIcon from "@material-ui/icons/Search";
import InputBase from "@material-ui/core/InputBase";
import IconButton from "@material-ui/core/IconButton";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    maxWidth: "400px", //mengatur panjang max searchbar
    padding: "2px", //mengatur lebar searchbar
    borderRadius: "20px", //membuat sudut oval
    alignItems: "center", //mengatur posisi kolom child
    marginTop: "15px", //mengatur jarak atas
    // marginBottom: "50px",
    paddingLeft: "24px",
    paddingRight: "24px",
    display: "flex",
    marginLeft: "20px",
    marginRight: "20px"
  },
  input: {
    width: "100px",
    height: "20px",
    marginLeft: "10px",
    fontSize: "0.875rem"
  },
  iconButton: {
    padding: 2,
    alignItems: "center"
  }
}));

export default function Search() {
  const classes = useStyles();
  return (
    <>
        <Grid container alignItems="center" justify="center">
          <Paper className={classes.root}>
            <IconButton className={classes.iconButton}>
              <SearchIcon />
            </IconButton>
            <InputBase className={classes.input} placeholder="Search..." />
          </Paper>
        </Grid>
    </>
  );
}