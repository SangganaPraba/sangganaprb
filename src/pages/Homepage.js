import React from "react";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import Searchbar from "../components/Searchbar";
import Card from "../components/Card";
import Categories from "../components/Categories";
import Banner from "../components/Banner";
import Products from "../components/Products";

function Homepage() {
  return (
    <>
     <Grid container alignItems="center" justify="center">
        <Grid
          style={{
            maxWidth: "360px",
            marginBottom: 30,
          }}
        >
          <div
            style={{
              backgroundImage:
                "url(" +
                "https://tumbasin.id/static/media/appbar.4aa6cb6f.svg" +
                ")",
              backgroundRepeat: "no-repeat",
              backgroundSize: "cover",
              maxHeight: "120px",
              top: "0"
            }}
          >
            <Searchbar />
            <Card />
          </div>
              <Typography
                style={{
                  marginTop: "40px",
                  marginLeft: "20px",
                  fontFamily: "Montserrat",
                  fontSize: "14px"
                }}
              >
                <b>Telusuri jenis produk</b>
              </Typography>
                <Grid
                  class="MuiGrid-root MuiGrid-container"
                  style={{
                  marginLeft: "10px",
                  borderRadius: "20px",
                  marginTop: "10px",
                  display: "flex"
                }}>
                <Categories/>
                </Grid>
              <Banner />
                <div style={{
                      color: "#14181B",
                      fontSize: "14px",
                      marginTop: "4px",
                      marginLeft: "12px",
                      marginBottom: "4px",
                      fontFamily: "Montserrat"
                    }}
                  >
                    <b>Produk Terlaris</b>
                </div>
                <Grid style={{
                  marginLeft: "100px"
                }}
                class="MuiGrid-root MuiGrid-item MuiGrid-grid-xs-9"
                >
                <div
                    align="right"
                    style={{
                      fontFamily: "Montserrat",
                      fontSize: "14px",
                      color: "#F15B5D",
                      marginLeft: "50px"
                    }}
                  >
                    Lihat semua
                </div>
                </Grid>
              <Products/>
        </Grid>
      </Grid>
    </>
  );
}

export default Homepage;