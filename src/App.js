import React from "react";
import "./App.css";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import Homepage from "../src/pages/Homepage";
import Login from "../src/components/Login";
import Help from "../src/components/Help";
import Bottomnav from "../src/components/Bottomnav";

function App() {
  return (
    <Router>
      <Switch>
        <Route exact path="/">
          <Homepage/>
          <Bottomnav/>
        </Route>
        <Route exact path="/login">
          <Login />
        </Route>
        <Route exact path="/help">
          <Help/>
          <Bottomnav/>
        </Route>
      </Switch>
    </Router>
  )
};
export default App;
